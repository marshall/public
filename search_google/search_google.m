% search_google: Google The Error Message
%
% search_google() - Opens a web browser and searches google
% for the last MATLAB error message
% search_google('some text') - Searches google for the typed text
%
% Inspired by the 'searcher' package for R.
% Written by Tom Marshall. www.tomrmarshall.com

function search_google(msgtext)

if nargin < 1
    msgtext = [ 'MATLAB "' lasterr '"'];
else
    msgtext = [ '"' msgtext '"'];
end

% replace carriage returns with spaces
returns = strfind(msgtext, newline);
msgtext(returns) = ' ';

preamble = 'https://google.com/search?q=';

web([preamble msgtext]);