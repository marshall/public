%% generate some data

d = [];
d{1,1} = randn(100,1);
d{2,1} = [randn(30,1)-1; randn(70,1) + 3];
d{3,1} = [randn(50,1); 4 + randn(50,1)];
d{1,2} = [abs(randn(100,1)) .^ 1.3];
d{2,2} = [2.5 + abs(randn(100,1)) .^ 1.3];
d{3,2} = [4 + abs(randn(100,1)) .^ 1.3];

%% colour info

cl = [];
cl(1,:) = [0 0.2 0.6];
cl(2,:) = [0.8 0.3 0.1];

%% plot figure

figure 
h = rm_raincloud(d, cl, 0, 'ks');

%% mess around with colours etc

cl(3,:) = [0.2 0.2 0.2];
h.p{2,2}.FaceColor = cl(3,:);
h.s{2,2}.MarkerFaceColor = cl(3,:);
h.m(2,2).MarkerFaceColor = cl(3,:);
h.s{2,2}.SizeData = 200;