% plots paired data with connecting lines
% use like: h = pairplot(data, cl)
% where data is an n x 2 matrix of paired data
% and cl is a 2 x 3 vector of colours
% optional third input argument changes dot size
%
% example usage
% x         = rand(10,2);               % generate some raw data
% cl        = [0.1 0.3 0.7; 0.4 0.8 1]; % colours
% figure; h = pairplot(x, cl);          % plot

function h = pairplot(data, cl, dotSize)

hold on

if nargin < 3
    dotSize = 120;
end

% set x-positions (y-positions are defined by the data)
dataSize    = size(data);
xPos        = zeros(dataSize);
xPos(:,1)   = 1;
xPos(:,2)   = 2;

% joining lines (plot these first so the dots are plotted over the top)
h.lines                 = line(xPos', data', 'Color', [0.2 0.2 0.2], 'LineWidth', 2);

% left-side dots
h.dots1                 = scatter(xPos(:,1), data(:,1));
h.dots1.MarkerEdgeColor = 'none';
h.dots1.MarkerFaceColor = cl(1,:);
h.dots1.SizeData        = dotSize;

% right-side dots
h.dots2                 = scatter(xPos(:,2), data(:,2));
h.dots2.MarkerEdgeColor = 'none';
h.dots2.MarkerFaceColor = cl(2,:);
h.dots2.SizeData        = dotSize;

% set x-axis so there is some space to the left and right of the dots
set(gca,'XLim', [0.8 2.2]);