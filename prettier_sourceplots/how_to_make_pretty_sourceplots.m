% Fieldtrip's default methods for plotting interpolated source data (in 
% 'slice' or 'ortho' form) can look a bit blocky, but a few very simple 
% tweaks to the masking parameter can make them look much nicer.

% TO-DO: Statistics? Mask significant blob with anatomy?

%% load anatomical MRI for interpolation
% I'm using the canonical single subject T1 included in fieldtrip, but this
% could be your subject's own MRI or another template

% replace this path with your fieldtrip path
path_to_ft  = '/Users/marshall/Documents/MATLAB/fieldtrip/';

T1dir       = '/template/anatomy/';
T1file      = 'single_subj_T1.nii';

T1          = ft_read_mri(fullfile(path_to_ft,T1dir,T1file));
T1.coordsys = 'mni';

%% load some un-interpolated functional data (some I made earlier)

% download 'sourcedata.mat' from https://git.fmrib.ox.ac.uk/marshall/public/tree/master/prettier_sourceplots
% directory you put it in
path_to_sourcedata = 'This path intentionally left blank';

sourcefile = 'sourcedata.mat';
load(fullfile(path_to_sourcedata,sourcefile));

%% interpolate functional data to template brain

cfg                 = [];
cfg.parameter       = 'powspctrm';
cfg.interpmethod    = 'spline';
func_interp         = ft_sourceinterpolate(cfg,func,T1);

func_interp.coordsys = 'mni'; 
% this data was computed using a grid from a single-subject warped to 
% MNI space - see here ( http://www.fieldtriptoolbox.org/example/create_single-subject_grids_in_individual_head_space_that_are_all_aligned_in_mni_space )

%% plot
% looks kinda blocky huh?

cfg                 = [];
cfg.method          = 'ortho';
cfg.funparameter    = 'powspctrm';
cfg.atlas           = fullfile(path_to_ft,'/template/atlas/aal/ROI_MNI_V4.nii');
ft_sourceplot(cfg,func_interp);

%% okay, we can make this prettier in a couple of ways
% first thing to do: don't display points outside the brain

% in order to do this we segment the brain from the anatomical template
cfg                 = [];
cfg.output          = {'brain'};
T1seg = ft_volumesegment(cfg,T1);

%% add this to the interpolated functional data

func_interp.brain_mask = T1seg.brain;

%% replot

cfg                 = [];
cfg.method          = 'ortho';
cfg.funparameter    = 'powspctrm';
cfg.atlas           = fullfile(path_to_ft,'/template/atlas/aal/ROI_MNI_V4.nii');
cfg.maskparameter   = 'brain_mask';
ft_sourceplot(cfg,func_interp);

% okay, we have an outline now, but we've lost the transparency and can no
% longer see the anatomy

%% another method, conjoin brainmask and functional data

func_interp.func_and_brain_mask = T1seg.brain .* func_interp.powspctrm;

% extra step because this mask is not a logical, but a scaled value, and
% fieldtrip doesn't like those structures to contain NaNs.
func_interp.func_and_brain_mask(isnan(func_interp.func_and_brain_mask)) = 0;

%% replot

cfg                 = [];
cfg.method          = 'ortho';
cfg.funparameter    = 'powspctrm';
cfg.atlas           = fullfile(path_to_ft,'/template/atlas/aal/ROI_MNI_V4.nii');
cfg.maskparameter   = 'func_and_brain_mask';
cfg.crosshair       = 'off';
ft_sourceplot(cfg,func_interp);

%% finally, my preferred option... conjoin brain mask and anatomy
% this produces a plot that has the same colour intensity throughout
% but still shows the anatomy

func_interp.anat_mask = T1seg.brain .* func_interp.anatomy;

%% replot

cfg                 = [];
cfg.method          = 'ortho';
cfg.funparameter    = 'powspctrm';
cfg.atlas           = fullfile(path_to_ft,'/template/atlas/aal/ROI_MNI_V4.nii');
cfg.maskparameter   = 'anat_mask';
cfg.crosshair       = 'off';
cfg.funcolorlim     = [-1.5 1.5];
ft_sourceplot(cfg,func_interp);

%%

func_interp.hard_mask = (func_interp.powspctrm > 0.5 | func_interp.powspctrm < -0.5) & func_interp.brain_mask;


cfg                 = [];
cfg.method          = 'ortho';
cfg.funparameter    = 'powspctrm';
cfg.atlas           = fullfile(path_to_ft,'/template/atlas/aal/ROI_MNI_V4.nii');
cfg.maskparameter   = 'hard_mask';
cfg.crosshair       = 'off';
cfg.funcolorlim     = [-1.5 1.5];
ft_sourceplot(cfg,func_interp);